# Descent

* Genre - Top down shooter
* Engine - Unreal Engine 4.27.2

## Controls

* WASD - Movement
* R - Reload weapon
* Z - Health ability
* 1, 2, 3, 4 - Change weapon

You can play in single and cooperative modes (type server - listen)

### [Demo](https://vl-wolf.itch.io/descent)
